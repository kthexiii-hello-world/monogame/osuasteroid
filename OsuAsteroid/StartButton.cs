﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using OsuAsteroid.Engine;

namespace OsuAsteroid {
    class StartButton : Button {
        public StartButton(String text, Texture2D bgTexture, SpriteFont font, Rectangle size) : base(text, bgTexture, font, size) { }
    }
}