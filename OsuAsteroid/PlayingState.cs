﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using OsuAsteroid.Engine;

namespace OsuAsteroid {
    class PlayingState : State {

        private int startAsteroidCount = 10;
        private int spaceShipCount = 6;

        private int baseMaxSpeed = 3;
        private int baseMinSpeed = 1;
        private int baseMaxSpaceShipSize = 69;
        private int baseMinSpaceShipSize = 20;
        private int baseMaxAsteroidSize = 169;
        private int splitMinimumSize = 110;
        private int baseMinAsteroidSize = 50;

        MouseState currentMouseState, previousMouseState;

        private double elapseTimeSinceSpawn = 0;
        private double spawnRate = 100.0d;

        private int playerHP = 20;
        private int maxDestroyed = 20;
        private int scoreMultiplier = 100;
        private int destroyedCount = 0;

        private Texture2D asteroidTexture;
        private Texture2D spaceShipTexture;

        List<GameObject> gameObjects;
        List<GameObject> objectsToBeAdded;

        Random rng;

        public PlayingState(Game1 game, GraphicsDevice graphicsDevice,
            ContentManager contentManager) : base(game, graphicsDevice, contentManager) {

            rng = new Random();
            gameObjects = new List<GameObject>();
            objectsToBeAdded = new List<GameObject>();

            asteroidTexture = contentManager.Load<Texture2D>("Icon");
            spaceShipTexture = contentManager.Load<Texture2D>("Square");

            for (int i = 0; i < spaceShipCount; i++) {
                int x = rng.Next(0, game.Window.ClientBounds.Width);
                int y = rng.Next(0, game.Window.ClientBounds.Height);
                int tmpScale = rng.Next(baseMinSpaceShipSize, baseMaxSpaceShipSize);
                gameObjects.Add(new SpaceShip("spaceship", new Vector2(x, y), spaceShipTexture, tmpScale, tmpScale));
            }

            for (int i = 0; i < startAsteroidCount; i++) {
                SpawnAsteroids();
            }

        }

        public override void Update(GameTime gameTime) {
            previousMouseState = currentMouseState;
            currentMouseState = Mouse.GetState();

            elapseTimeSinceSpawn += gameTime.ElapsedGameTime.TotalMilliseconds;

            if (elapseTimeSinceSpawn > spawnRate) {
                SpawnAsteroids();
                elapseTimeSinceSpawn = 0.0d;
            }

            foreach (GameObject o in gameObjects) {
                o.Update(gameTime);

                if (o.Name == "asteroid") {
                    Asteroid tmp = o as Asteroid;

                    if (tmp.IsAlive() && currentMouseState.LeftButton == ButtonState.Pressed &&
                        previousMouseState.LeftButton == ButtonState.Released) {

                        if (tmp.Contains(currentMouseState.X, currentMouseState.Y)) {
                            if (tmp.GetSize() > splitMinimumSize) {
                                SpawnSplitAsteroid((int) tmp.Position.X, (int) tmp.Position.Y, tmp.GetSize() / 2);
                                SpawnSplitAsteroid((int) tmp.Position.X, (int) tmp.Position.Y, tmp.GetSize() / 2);
                            }

                            destroyedCount++;

                            tmp.Destroy();
                        }
                    }

                    if (!(tmp.Position.X > -baseMaxAsteroidSize * 2 && tmp.Position.X < game.Window.ClientBounds.Width + baseMaxAsteroidSize * 2 &&
                            tmp.Position.Y > -baseMaxAsteroidSize * 2 && tmp.Position.Y < game.Window.ClientBounds.Height + baseMaxAsteroidSize * 2)) {

                        playerHP--;
                        tmp.Destroy();
                    }

                    foreach (GameObject a in gameObjects) {
                        if (a.Name == "asteroid" && o != a) {
                            Asteroid tmpB = a as Asteroid;

                            if (tmp.Contains((int) tmpB.Position.X - tmpB.Width / 2, (int) tmpB.Position.Y + tmpB.Height / 2)) {
                                if (tmp.GetSize() > splitMinimumSize) {
                                    SpawnSplitAsteroid((int) tmp.Position.X, (int) tmp.Position.Y, tmp.GetSize() / 2);
                                    SpawnSplitAsteroid((int) tmp.Position.X, (int) tmp.Position.Y, tmp.GetSize() / 2);
                                }

                                if (tmpB.GetSize() > splitMinimumSize) {
                                    SpawnSplitAsteroid((int) tmpB.Position.X, (int) tmpB.Position.Y, tmpB.GetSize() / 2);
                                    SpawnSplitAsteroid((int) tmpB.Position.X, (int) tmpB.Position.Y, tmpB.GetSize() / 2);
                                }

                                tmp.Destroy();
                                tmpB.Destroy();
                            }
                        }
                    }
                }

            }
        }

        public override void PostUpdate(GameTime gameTime) {

            for (int i = gameObjects.Count - 1; i >= 0; i--) {
                GameObject tmp = gameObjects[i];

                if (tmp.Name == "asteroid") {
                    Asteroid tmpCast = tmp as Asteroid;
                    if (!tmpCast.IsAlive()) {
                        gameObjects.RemoveAt(i);
                    }
                }
            }

            if (objectsToBeAdded.Count > 1) {
                for (int i = 0; i < objectsToBeAdded.Count; i++) {
                    gameObjects.Add(objectsToBeAdded[i]);
                }
                objectsToBeAdded.Clear();
            }

            gameObjects.TrimExcess();

            game.SetTitle("HP: " + playerHP + ", score: " + destroyedCount * scoreMultiplier);

            if (playerHP < 1 || destroyedCount > maxDestroyed - 1) {
                RestartState tmp = new RestartState(game, graphicsDevice, contentManager);
                if (playerHP < 1) tmp.SetBackground(Color.Red);
                else tmp.SetBackground(Color.Yellow);
                game.SetNextState(tmp);
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
            spriteBatch.Begin();

            foreach (GameObject o in gameObjects)
                o.Draw(gameTime, spriteBatch);

            spriteBatch.End();
        }

        private void SpawnSplitAsteroid(int x, int y, int scale) {
            float vx = (rng.Next(baseMinSpeed, baseMaxSpeed)) * (rng.NextDouble() > 0.5 ? -1 : 1);
            float vy = (rng.Next(baseMinSpeed, baseMaxSpeed)) * (rng.NextDouble() > 0.5 ? -1 : 1);
            Asteroid tmpAsteroid = new Asteroid("asteroid", new Vector2(x, y), asteroidTexture, scale, scale);

            tmpAsteroid.Velocity = new Vector2(vx, vy);
            objectsToBeAdded.Add(tmpAsteroid);
        }

        private void SpawnAsteroids() {
            int randomSide = rng.Next(0, 4);
            int y = 0, x = 0;
            float vx = 0, vy = 0;

            switch (randomSide) {
                case 0:
                case 2:
                    y = rng.Next((int) (game.Window.ClientBounds.Height * 0.2), game.Window.ClientBounds.Height - (int) (game.Window.ClientBounds.Height * 0.2));
                    x = randomSide == 0 ? -baseMaxAsteroidSize : game.Window.ClientBounds.Width + baseMaxAsteroidSize;

                    vx = rng.Next(baseMinSpeed, baseMaxSpeed) * (randomSide == 0 ? 1 : -1);
                    vy = rng.Next(baseMinSpeed, (int) (baseMaxSpeed * 0.5)) * (rng.NextDouble() > 0.5 ? -1 : 1);
                    break;
                case 1:
                case 3:
                    x = rng.Next(0, game.Window.ClientBounds.Width);
                    y = randomSide == 1 ? -baseMaxAsteroidSize : game.Window.ClientBounds.Height + baseMaxAsteroidSize;

                    vx = rng.Next(baseMinSpeed, (int) (baseMaxSpeed * 0.5)) * (rng.NextDouble() > 0.5 ? -1 : 1);
                    vy = rng.Next(baseMinSpeed, baseMaxSpeed) * (randomSide == 1 ? 1 : -1);
                    break;
                default:
                    break;
            }

            int tmpScale = rng.Next(baseMinAsteroidSize, baseMaxAsteroidSize);

            Asteroid tmpAsteroid = new Asteroid("asteroid", new Vector2((int) x, (int) y), asteroidTexture, tmpScale, tmpScale);

            tmpAsteroid.Velocity = new Vector2(vx, vy);
            gameObjects.Add(tmpAsteroid);
        }

    }
}