﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using OsuAsteroid.Engine;

namespace OsuAsteroid {
    class SpaceShip : GameObject {
        public SpaceShip(String name, Vector2 position, Texture2D texture, int width, int height) : base(name, position, texture, width, height) {

        }
        public override void Update(GameTime gameTime) {

        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
            base.Draw(gameTime, spriteBatch);
            GetRect();
        }
    }
}