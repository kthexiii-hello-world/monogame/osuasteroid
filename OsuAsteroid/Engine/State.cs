using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace OsuAsteroid.Engine {
    public abstract class State {

        protected ContentManager contentManager;

        protected GraphicsDevice graphicsDevice;

        protected Game1 game;

        public abstract void Update(GameTime gameTime);

        public abstract void PostUpdate(GameTime gameTime);

        public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);

        public State(Game1 game, GraphicsDevice graphicsDevice, ContentManager contentManager) {
            this.game = game;
            this.graphicsDevice = graphicsDevice;
            this.contentManager = contentManager;
        }

    }
}