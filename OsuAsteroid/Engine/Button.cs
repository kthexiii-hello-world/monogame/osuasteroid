﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace OsuAsteroid.Engine {
    public abstract class Button {
        protected Texture2D buttonTexture;
        protected SpriteFont font;
        protected Rectangle Size;
        protected Color bgColor;
        protected Color fgColor;

        public String ButtonText;

        public Button(String text, Texture2D bgTexture, SpriteFont font, Rectangle size) {
            buttonTexture = bgTexture;
            this.font = font;
            Size = size;
            ButtonText = text;

            bgColor = Color.White;
            fgColor = Color.Black;
        }

        public bool IsPressed() {
            return Size.Contains(Input.GetMouseX(), Input.GetMouseY()) && Input.IsLeftButtonPressed();
        }

        public void Update(GameTime gameTime) {

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
            spriteBatch.Draw(buttonTexture, Size, bgColor);
            spriteBatch.DrawString(font, ButtonText, new Vector2(Size.X + Size.Width / 2 - font.MeasureString(ButtonText).X / 2,
                Size.Y + Size.Height / 2 - font.MeasureString(ButtonText).Y / 2), fgColor, 0f, Vector2.Zero, 1.0f, SpriteEffects.None, 1f);
        }

        public void ChangeColor(Color color) {
            fgColor = color;
        }

        public void ChangBGColor(Color color) {
            bgColor = color;

        }
    }
}