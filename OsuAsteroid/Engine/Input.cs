using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace OsuAsteroid.Engine {
    public static class Input {
        static MouseState currentMouseState, previousMouseState;

        public static void Update() {
            previousMouseState = currentMouseState;
            currentMouseState = Mouse.GetState();
        }

        public static bool IsLeftButtonPressed() {
            return currentMouseState.LeftButton == ButtonState.Pressed && previousMouseState.LeftButton == ButtonState.Released;
        }

        public static int GetMouseX() {
            return currentMouseState.X;
        }

        public static int GetMouseY() {
            return currentMouseState.Y;
        }

    }

}