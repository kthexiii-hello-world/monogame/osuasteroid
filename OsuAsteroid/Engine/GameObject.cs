﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OsuAsteroid.Engine {
    public abstract class GameObject {
        public Vector2 Position;
        public Vector2 Velocity = Vector2.Zero;
        public Vector2 Acceleration = Vector2.Zero;

        protected Texture2D texture;
        protected Color color;

        public int Width, Height;

        public String Name;

        protected uint id;
        protected static uint IDIndex = 0;

        public GameObject(String name, Vector2 position, Texture2D texture, int width, int height) {
            Position = position;
            this.texture = texture;
            Name = name;

            id = IDIndex;
            IDIndex++;

            Width = width;
            Height = height;
            color = Color.White;
        }

        public void SetColor(Color color) {
            this.color = color;
        }

        public abstract void Update(GameTime gameTime);

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
            spriteBatch.Draw(texture, GetRect(), color);
        }

        protected Rectangle GetRect() {
            return new Rectangle((int) Position.X, (int) Position.Y, Width, Height);
        }
    }
}