using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using OsuAsteroid.Engine;

namespace OsuAsteroid {
    public class RestartState : State {

        private Button btn;

        private Texture2D bg;

        private SpriteFont font;

        private Color clearColor;

        public RestartState(Game1 game, GraphicsDevice graphicsDevice, ContentManager contentManager) : base(game, graphicsDevice, contentManager) {
            btn = new StartButton("RESTART", contentManager.Load<Texture2D>("Square"),
                contentManager.Load<SpriteFont>("DefaultFont"),
                new Rectangle(game.Window.ClientBounds.Width / 2 - 100 / 2, game.Window.ClientBounds.Height / 2 - 50 / 2, 100, 50));

            font = contentManager.Load<SpriteFont>("DefaultFont");
            clearColor = Color.Black;
        }

        public void SetText(string text) {

        }

        public void SetBackground(Color color) {
            clearColor = color;
        }

        public override void Update(GameTime gameTime) {

            btn.Update(gameTime);

            if (btn.IsPressed()) {
                game.SetNextState(new PlayingState(game, graphicsDevice, contentManager));
            }

        }

        public override void PostUpdate(GameTime gameTime) {

        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
            graphicsDevice.Clear(clearColor);

            spriteBatch.Begin();

            btn.Draw(gameTime, spriteBatch);

            spriteBatch.End();
        }

    }

}