﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using OsuAsteroid.Engine;

namespace OsuAsteroid {
    class Asteroid : GameObject {
        private bool alive = true;

        public Asteroid(String name, Vector2 position, Texture2D texture, int width, int height) : base(name, position, texture, width, height) {

        }

        public bool IsAlive() {
            return alive;
        }

        public void Destroy() {
            alive = false;
        }

        public bool Contains(int x, int y) {
            return GetRect().Contains(x, y);
        }

        public int GetSize() {
            return GetRect().Width;
        }

        public override void Update(GameTime gameTime) {
            Position += Velocity;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
            base.Draw(gameTime, spriteBatch);
        }
    }
}