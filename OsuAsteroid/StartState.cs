﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using OsuAsteroid.Engine;

namespace OsuAsteroid {
    class StartState : State {

        Texture2D square;

        Button startButton;

        public StartState(Game1 game, GraphicsDevice graphicsDevice, ContentManager contentManager) : base(game, graphicsDevice, contentManager) {
            startButton = new StartButton("START", contentManager.Load<Texture2D>("Square"),
                contentManager.Load<SpriteFont>("DefaultFont"),
                new Rectangle(game.Window.ClientBounds.Width / 2 - 100 / 2, game.Window.ClientBounds.Height / 2 - 50 / 2, 100, 50));

        }

        public override void Update(GameTime gameTime) {
            startButton.Update(gameTime);

            if (startButton.IsPressed()) {
                game.SetNextState(new PlayingState(game, graphicsDevice, contentManager));
                // game.SetNextState(new RestartState(game, graphicsDevice, contentManager));
            }

        }

        public override void PostUpdate(GameTime gameTime) { }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
            spriteBatch.Begin();

            startButton.Draw(gameTime, spriteBatch);

            spriteBatch.End();
        }

    }
}