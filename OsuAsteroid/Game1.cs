﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using OsuAsteroid.Engine;

namespace OsuAsteroid {
    public class Game1 : Game {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private State currentState;
        private State nextState;

        public Game1() {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize() {
            base.Initialize();
        }

        protected override void LoadContent() {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            currentState = new StartState(this, _graphics.GraphicsDevice, Content);
        }

        protected override void Update(GameTime gameTime) {
            Input.Update();

            if (nextState != null) {
                currentState = nextState;
                nextState = null;
            }

            currentState.Update(gameTime);
            currentState.PostUpdate(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            currentState.Draw(gameTime, _spriteBatch);

            base.Draw(gameTime);
        }

        public void SetNextState(State state) {
            nextState = state;
        }

        public void SetTitle(string text) {
            Window.Title = text;
        }
    }
}